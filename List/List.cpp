#include <iostream>
#include <cstdlib>
#include "List.hpp"

#define DEBUG 1

List::List(size_t size) {
    this->pointer = (size_t*)malloc(sizeof(size_t) * size);
    this->current = 0;
    this->size = size;

    if ( DEBUG ) {
        std::cout << "List::List(size_t size=0)" << std::endl;
    }
}

List::List(const List& lst) {
    this->pointer = (size_t*)malloc(sizeof(size_t) * lst.size);
    this->current = lst.current;
    this->size = lst.size;

    for ( size_t i = 0; i < this->current; i++ ) {
        this->pointer[i] = lst.pointer[i];
    }

    if ( DEBUG ) {
        std::cout << "List::List(const List& lst)" << std::endl;
    }
};

void List::operator=(const List& lst) {
    size_t* temp;

    temp = (size_t*)realloc(this->pointer, sizeof(size_t) * lst.size);

    if ( temp == NULL ) {
        temp = (size_t*)malloc(sizeof(size_t) * lst.size);
    }

    if ( temp == NULL ) {
        throw AllocationException();
    }

    this->pointer = temp;
    this->current = lst.current;
    this->size = lst.size;

    for ( size_t i = 0; i < this->current; i++ ) {
        this->pointer[i] = lst.pointer[i];
    }

    if ( DEBUG ) {
        std::cout << "List::operator=(const List& lst)" << std::endl;
    }
}

size_t& List::operator[](size_t index) {
    if ( index < current ) {
        return this->pointer[index];
    } else {
        throw IndexException();
    }
}

size_t& List::operator*() {
    if ( current <= 0 ) {
        throw IndexException();
    }

    return this->pointer[current - 1];
}

void List::operator()() {
    this->~List();
}

void List::report() const {
    std::cout << "-----------" << std::endl;
    std::cout << "Pointer is: " << this->pointer << std::endl;
    std::cout << "Size is: " << this->size << std::endl;
    std::cout << "Current is: " << this->current << std::endl;
    std::cout << "-----------" << std::endl;

    if ( DEBUG ) {
        std::cout << "List::report()" << std::endl;
    }
}

void List::zeroFill() {
    for ( size_t i = 0; i < this->size; i++ ) {
        this->pointer[i] = 0;
    }

    if ( DEBUG ) {
        std::cout << "List::zeroFill()" << std::endl;
    }
}

void List::clear() {
    this->zeroFill();
    this->current = 0;
}

size_t List::elementCount(size_t element) const {
    size_t counter = 0;

    for ( size_t i = 0; i < this->current; i++ ) {
        if ( this->pointer[i] == element ) {
            counter += 1;
        }
    }

    return counter;
}

void List::expand(size_t newSize) {
    size_t* temp;
    newSize = newSize + this->size / 5;

    temp = (size_t*)realloc(this->pointer, sizeof(size_t) * newSize);

    if ( temp != NULL ) {
        this->pointer = temp;
        this->size = newSize;
    } else {
        throw AllocationException();
    }
}

void List::add(size_t value, size_t amount) {
    size_t newSize = this->current + amount;

    if ( newSize > this->size ) {
        this->expand(newSize);
    }

    for ( size_t i = 0; i < amount; i++ ) {
        this->pointer[this->current] = value;
        this->current += 1;
    }
}
void List::print() const {
    size_t last = this->current - 1;

    for ( size_t i = 0; i < last; i++ ) {
        std::cout << this->pointer[i] << ' ';
    }
    std::cout << this->pointer[last] << std::endl;

    if ( DEBUG ) {
        std::cout << "printing\n" << std::endl;
    }
}

List::~List() {
    free(this->pointer);
    this->size = 0;
    this->pointer = NULL;

    if( DEBUG ) {
        std::cout << "List::~List()" << std::endl;
    }
}

std::ostream& operator<<(std::ostream& out, List& lst) {
    size_t last = lst.current - 1;

    for ( size_t i = 0; i < last; i++ ) {
        out << lst[i] << ' ';
    }
    out << lst[last] << std::endl;

    return out;
}
