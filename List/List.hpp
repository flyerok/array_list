class AllocationException {};
class IndexException {};

class List {
private:
    size_t* pointer;
    size_t current;
    size_t size;

public:
    friend std::ostream& operator<<(std::ostream&, List&);

    List(size_t size=0);
    List(const List&);
    void operator=(const List&);

    size_t& operator[](size_t);
    size_t& operator*();
    void operator()();
    
    void report() const;
    void zeroFill();
    void clear();
    size_t elementCount(size_t element) const;
    void expand(size_t newSize);
    void add(size_t value, size_t amount=1);
    void print() const;

    ~List();
};

std::ostream& operator<<(std::ostream&, List&);
