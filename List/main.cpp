#include <iostream>
#include "List.hpp"

int main() {
    List* rawList = new List();
    List& list = *rawList;
    List list2;

    list.report();

    try {
        list.add(5, 80);
    } catch ( AllocationException ) {
        std::cout << "Shuting down..." << std::endl;
        return 42;
    }
    
    std::cout << list << std::endl;
    // list();

    std::cout << "Count " << list.elementCount(4) << std::endl;

    list[1] = 10;

    list2 = list;

    // list.print();
    list.report();
    list.clear();
    list.report();

    delete rawList;

    try {
        size_t value = list[2];

        std::cout << "INDEX: " << value << std::endl;
    } catch (IndexException) {
        std::cout << "Index is currepted." << std::endl;
    }
    
    list.report();
    list2.report();

    try {
        size_t value = *list2;

        std::cout << "VALUE: " << value << std::endl;
    } catch (IndexException) {
        std::cout << "Index is currepted." << std::endl;
    }



    return 0;
}
