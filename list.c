#include <stdio.h>
#include <stdlib.h>

#define SIZE 10
#define DEBUG 0

typedef struct {
    int* pointer;
    int current;
    int size;
} ArrayList;

void listInit(ArrayList* list, int size) {
    list->pointer = (int*)malloc(sizeof(int) * size);
    list->current = 0;
    list->size = size;

    if ( DEBUG ) {
        printf("init\n");
    }
}

void listReport(ArrayList* list) {
    printf("-----------\n");
    printf("Pointer is: %p\n", list->pointer);
    printf("Size is: %d\n", list->size);
    printf("Current is: %d\n", list->current);
    printf("-----------\n");

    if ( DEBUG ) {
        printf("report\n");
    }
}

void listZeroFill(ArrayList* list) {
    for ( int i = 0; i < list->size; i++ ) {
        list->pointer[i] = 0;
    }

    if ( DEBUG ) {
        printf("filling by zero\n");
    }
}

void listClear(ArrayList* list) {
    listZeroFill(list);
    list->current = 0;
}

int listElementCount(ArrayList* list, int element) {
    int counter = 0;

    for ( int i = 0; i < list->current; i++ ) {
        if ( list->pointer[i] == element ) {
            counter += 1;
        }
    }

    return counter;
}

void expandList(ArrayList* list, int newSize) {
    int* temp;
    newSize = newSize + list->size / 5;

    temp = (int*)realloc(list->pointer, sizeof(int) * newSize);

    if ( temp != NULL ) {
        list->pointer = temp;
        list->size = newSize;
    } else {
        printf("Realloc is failed\n");
    }
}

void listAdd(ArrayList* list, int value, int amount) {
    int newSize = list->current + amount;

    if ( newSize > list->size ) {
        expandList(list, newSize);
    }

    for ( int i = 0; i < amount; i++ ) {
        list->pointer[list->current] = value;
        list->current += 1;
    }
}

void listPrint(ArrayList* list) {
    int last = list->current - 1;

    for ( int i = 0; i < last; i++ ) {
        printf("%d ", list->pointer[i]);
    }
    printf("%d\n", list->pointer[last]);

    if ( DEBUG ) {
        printf("printing\n");
    }
}

void listDestructor(ArrayList* list) {
    free(list->pointer);
    list->size = 0;
    list->pointer = NULL;
}

int main() {
    ArrayList list;

    listInit(&list, SIZE);
    listReport(&list);
    listAdd(&list, 5, 80);
    printf("Count %d\n", listElementCount(&list, 4));
    listPrint(&list);
    listReport(&list);
    listClear(&list);
    listReport(&list);
    listDestructor(&list);
    listReport(&list);

    return 0;
}
